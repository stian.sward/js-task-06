import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import GuardedRoute from 'react-router-guards';
import Header from './components/Header';

import LoginPage from './views/LoginPage';
import ProfilePage from './views/ProfilePage.jsx';
import TranslatorPage from './views/TranslatorPage';

function App() {
  return (
    <Router>
      <Header/>
      <Switch>
        <Route path="/login" component={LoginPage}/>
        <GuardedRoute path="/profile" component={ProfilePage} auth={isLoggedIn}/>
        <GuardedRoute exact path="" component={TranslatorPage} auth={isLoggedIn}/>
      </Switch>
    </Router>
  );
}

export default App;
